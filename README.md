# TP J2EE Spring Boot + Hibernate

L'Objectif de ce TP a été d'écrire une petite application de gestion des inscriptions de participants à des évenements en utilisant J2EE ainsi que le Framework Spring pour l’architecture MVC et Hibernate pour la couche de persitance.

L'application créée comprend les fonctionnalités suivantes:
- Affichage de la liste des participants
- Affichage des évenement
- Ajout de participant dans la liste avec association à un évenement
- Ajout d'évenement dans la liste
- Modification des information au sein d'un participant dans la liste
- Suppression du participant de la liste
- Suppression d'un évenement avec les participants associés

L'application se connecte à une base de données de type PostGreSQL pour permettre à l'application d'interagir avec les objets de persistance, à savoir, les participants et les évenements, les tableaux s'organisent de la façon suivante:

Pour les participants:
 - Id
 - Nom
 - Prénom
 - Adresse email
 - Evenement choisi 

Pour les évenements:
 - Id
 - Intitulé
 - Date de début de la forme "JJMMAAAA"
 - Date de fin de la forme "JJMMAAAA"

Dans ce contexte, un évenement peut accueillir plusieurs participants et une personne peut participer à un seul évenement. 

## Lancement de l'application

Pour lancer l'application, ouvrir le projet avec Eclipse J2E et lancer la commande d'exécution Java Application vers la classe "GestionApplication". L'application peut bien lancer dans un terminal Linux, avec l'environnement Java J2EE, Maven, Hibernate, Spring et PostgreSQL installés. Dans ce cas, lancer la commande suivante dans le répertoire où est situé le fichier pom.xml:
```
./mvnw spring-boot:run
```

Une fois Spring démarré, ouvrir une page web avec l'adresse url suivante:
```
localhost:8080
```

L'application fonctionne lorsque la page web s'affiche correctement avec les lien représentant les fonctionnalités associées.

Projet Réalisé par Stéphane Wilhelm dans le cadre de la formation M2 TSI à l'ENSG