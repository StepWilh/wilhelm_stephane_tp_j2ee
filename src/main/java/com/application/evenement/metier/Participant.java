package com.application.evenement.metier;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="participants")
public class Participant {
	
	@Id
	@GeneratedValue(generator="increment")
	//@GenericGenerator(name="increment", strategy="increment")
	@Column(name="num_pers")
	private int num_pers;
	
	@Column(name="nom")
	private String nom;
	
	@Column(name="prenom")
	private String prenom;
	
	@Column(name="email")
	private String email;
	
	@ManyToOne
	@JoinColumn(name="even_part")
	private Evenement evenement;

	public int getNum_pers() {
		return num_pers;
	}

	public void setNum_pers(int num_pers) {
		this.num_pers = num_pers;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Evenement getEvenement() {
		return evenement;
	}

	public void setEvenement(Evenement evenement) {
		this.evenement = evenement;
	}
	
	
}
