package com.application.evenement.metier;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

public interface EvenementRepository extends CrudRepository<Evenement,Integer>{

}
