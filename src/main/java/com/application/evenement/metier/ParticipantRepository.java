package com.application.evenement.metier;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

public interface ParticipantRepository extends CrudRepository<Participant, Integer>{

}
