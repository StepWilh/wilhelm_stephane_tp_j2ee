package com.application.evenement.metier;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="evenements")
public class Evenement {
	
	@Id
	@GeneratedValue(generator="increment")
	//@GenericGenerator(name="increment", strategy="increment")
	@Column(name="num_even")
	private int num_even;
	
	@Column(name="intitule")
	private String intitule;
	
	@Column(name="date_debut")
	private String date_debut;
	
	@Column(name="date_fin")
	private String date_fin;
	
	@OneToMany(mappedBy="evenement")
	private List<Participant> participants = new ArrayList<>();

	public int getNum_even() {
		return num_even;
	}

	public void setNum_even(int num_even) {
		this.num_even = num_even;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public String getDate_debut() {
		return date_debut;
	}

	public void setDate_debut(String date_debut) {
		this.date_debut = date_debut;
	}

	public String getDate_fin() {
		return date_fin;
	}

	public void setDate_fin(String date_fin) {
		this.date_fin = date_fin;
	}

}
